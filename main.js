
// Load our libraries
var express = require("express");
var path = require("path");

// console.info(">>> express: "+ express);
// console.info(">>>path: " + path);

// Create instance of express application
var app = express();

// Define routes
app.use(express.static(__dirname + "/public"));
// console.info(">> __dirname: " + __dirname);

app.use(function(req,resp) {
    resp.status(404);
    resp.type("text/html"); // Representation of resource
    resp.send("<h1>File not found. </h1><p>The current time is " + new Date() + "</p>");
});

// Setting the port as a property of the app
app.set("port",3000);

// console.info("Port: " + app.get("port"));

// Start the server on the specified port
app.listen(app.get("port"), function() {
    console.info("Application is listening on port " + app.get("port"));
    console.info("Yaaah ...");
});



